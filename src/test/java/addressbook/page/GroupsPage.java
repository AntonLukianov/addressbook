package addressbook.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class GroupsPage {
    private WebDriver driver;
    public GroupsPage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickButtonNewGroup() {
        WebElement buttonNewGroup = driver.findElement(By.xpath("//input[@name=\"new\"]"));
        buttonNewGroup.click();
    }

    public void checkAddGroup(String text) {
        Assert.assertEquals(driver.getPageSource().contains(text), true);
    }

    public void deleteGroup() {
        WebElement checkBoxGroup = driver.findElement(By.xpath("//input[@title=\"Select (test)\"]"));
        checkBoxGroup.click();
        WebElement buttonDeleteGroup = driver.findElement(By.xpath("//input[@name=\"delete\"]"));
        buttonDeleteGroup.click();
        Assert.assertTrue(driver.getPageSource().contains("Group has been removed."));
    }

    public void checkDeleteGroup(String text) {
        Assert.assertFalse(driver.getPageSource().contains(text));
    }


}
