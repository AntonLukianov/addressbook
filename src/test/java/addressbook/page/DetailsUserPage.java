package addressbook.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class DetailsUserPage {
    private WebDriver driver;
    public DetailsUserPage(WebDriver driver) {
        this.driver = driver;
    }

    public void checkDetails() {
        Assert.assertTrue(driver.getPageSource().contains("Member of: "));
        String groupName = driver.findElement(By.xpath("//a[@href=\"./?group=test\"]")).getText();
        Assert.assertTrue(groupName.equals("test"));

    }

}
