package addressbook.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.ArrayList;

public class AddEditNewUserForm {
    private WebDriver driver;
    public AddEditNewUserForm(WebDriver driver) {
        this.driver = driver;
    }




    public void sendDataUser(String name, String surname, String address, String telMob, String email, String dayB, String monthB, String yearB) {
        WebElement fname = driver.findElement(By.xpath("//input[@name=\"firstname\"]"));
        WebElement sname = driver.findElement(By.xpath("//input[@name=\"lastname\"]"));
        WebElement addr = driver.findElement(By.xpath("//textarea[@name=\"address\"]"));
        WebElement mob = driver.findElement(By.xpath("//input[@name=\"mobile\"]"));
        WebElement mail = driver.findElement(By.xpath("//input[@name=\"email\"]"));
        WebElement day = driver.findElement(By.xpath("//select[@name=\"bday\"]"));
        WebElement month = driver.findElement(By.xpath("//select[@name=\"bmonth\"]"));
        WebElement year = driver.findElement(By.xpath("//input[@name=\"byear\"]"));
        fname.sendKeys(name);
        sname.sendKeys(surname);
        addr.sendKeys(address);
        mob.sendKeys(telMob);
        mail.sendKeys(email);
        day.sendKeys(dayB);
        month.sendKeys(monthB);
        year.sendKeys(yearB);
    }

    public void assertEditAndParam(String name, String surname, String address, String telMob, String email, String dayB, String monthB, String yearB) {
        String fname = driver.findElement(By.xpath("//input[@name=\"firstname\"]")).getAttribute("value");
        String sname = driver.findElement(By.xpath("//input[@name=\"lastname\"]")).getAttribute("value");
        String addr = driver.findElement(By.xpath("//textarea[@name=\"address\"]")).getText();
        String mob = driver.findElement(By.xpath("//input[@name=\"mobile\"]")).getAttribute("value");
        String mail = driver.findElement(By.xpath("//input[@name=\"email\"]")).getAttribute("value");
        String day = driver.findElement(By.xpath("//select[@name=\"bday\"]")).getAttribute("value");
        String month = driver.findElement(By.xpath("//select[@name=\"bmonth\"]")).getAttribute("value");
        String year = driver.findElement(By.xpath("//input[@name=\"byear\"]")).getAttribute("value");

        Assert.assertTrue(fname.equals(name));
        Assert.assertTrue(sname.equals(surname));
        Assert.assertTrue(addr.equals(address));
        Assert.assertTrue(mob.equals(telMob));
        Assert.assertTrue(mail.equals(email));
        Assert.assertTrue(day.equals(dayB));
        Assert.assertTrue(month.equals(monthB));
        Assert.assertTrue(year.equals(yearB));

    }

    public void clickButtonEnter() {
        WebElement buttonEnter = driver.findElement(By.xpath("//input[@value=\"Enter\"]"));
        buttonEnter.click();
    }

    public void clickButtonDelete() {
        WebElement buttonDelete = driver.findElement(By.xpath("//input[@value=\"Delete\"]"));
        buttonDelete.click();
    }

    public void checkAddUser(String text) {
        Assert.assertTrue(driver.getPageSource().contains(text));
    }

    public void checkDeleteUser(String text) {
        Assert.assertFalse(driver.getPageSource().contains(text));
    }


}
