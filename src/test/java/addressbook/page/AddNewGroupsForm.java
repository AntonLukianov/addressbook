package addressbook.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AddNewGroupsForm {
    private WebDriver driver;
    public AddNewGroupsForm(WebDriver driver) {
        this.driver = driver;
    }



    public void addGroup(String nameGroup) {
        WebElement nGroup = driver.findElement(By.xpath("//input[@name=\"group_name\"]"));
        nGroup.sendKeys(nameGroup);
        WebElement buttonEnterInfo = driver.findElement(By.xpath("//input[@name=\"submit\"]"));
        buttonEnterInfo.click();

    }
}
