package addressbook.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {
    private WebDriver driver;
    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public String getTitle() {
        return driver.getTitle();

    }



    public void clickEditUser() {
        WebElement editUser = driver.findElement(By.xpath("//img[@title=\"Edit\"]"));
        editUser.click();
    }

    public void clickDetailsUser() {
        WebElement detailsUser = driver.findElement(By.xpath("//img[@title=\"Details\"]"));
        detailsUser.click();
    }

    public void addUserToGroup(){
       WebElement checkBoxUser = driver.findElement(By.xpath("//input[@title=\"Select (Boris Ivanov)\"]"));
       checkBoxUser.click();
       WebElement selectGroup = driver.findElement(By.xpath("//select[@name=\"to_group\"]"));
       selectGroup.sendKeys("test");
       WebElement buttonAddToGroup = driver.findElement(By.xpath("//input[@name=\"add\"]"));
       buttonAddToGroup.click();
    }



}
