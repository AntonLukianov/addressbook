package addressbook;

import addressbook.page.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

public class TestAddressBook {
    private WebDriver driver;

    //настройка драйвера
    @BeforeSuite
    public void setUp() {
        System.setProperty(
                "webdriver.chrome.driver",
                "chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    //Проверяем, открылась ли стартовая страница
    @Test(priority = 0, description = "First test")
    public void openHomePage() {
        driver.get("http://localhost/addressbook/");
        HomePage homePage = new HomePage(driver);
        String title =  homePage.getTitle();
        Assert.assertTrue(title.equals("Address book"));
    }

    @Parameters({"name","surname","address","telMob","email","dayB","monthB","yearB"}) //подключаем параметры из xml
    @Test(priority = 1, description = "Second test")
    public void addNewUser(String name, String surname, String address, String telMob, String email, String dayB, String monthB, String yearB) {
        driver.get("http://localhost/addressbook/edit.php");
        AddEditNewUserForm addEditNewUserForm = new AddEditNewUserForm(driver);
        //добавляем пользователя
        addEditNewUserForm.sendDataUser(name, surname, address, telMob, email, dayB, monthB, yearB);
        addEditNewUserForm.clickButtonEnter();
        driver.get("http://localhost/addressbook/");
        HomePage homePage = new HomePage(driver);
        //проверяем, есть ли в списке пользователь и все ли поля соответствуют введенным данным
        addEditNewUserForm.checkAddUser("Select (Boris Ivanov)");
        homePage.clickEditUser();
        addEditNewUserForm.assertEditAndParam(name, surname, address, telMob, email, dayB, monthB, yearB);

    }

    @Test(priority = 2, description = "Third test")
    public void addNewGroup() {
        driver.get("http://localhost/addressbook/group.php");
        GroupsPage groupsPage = new GroupsPage(driver);
        //добавляем новую группу
        groupsPage.clickButtonNewGroup();
        AddNewGroupsForm addNewGroupsForm = new AddNewGroupsForm(driver);
        addNewGroupsForm.addGroup("test");
        driver.get("http://localhost/addressbook/group.php");
        //проверяем, появилась ли группа в списке
        groupsPage.checkAddGroup("Select (test)");
    }


    @Test(priority = 3, description = "Fourth test")
    public void addToGroup() {
        driver.get("http://localhost/addressbook/");
        HomePage homePage = new HomePage(driver);
        //добавляем пользователя в группу test
        homePage.addUserToGroup();
        driver.get("http://localhost/addressbook/");
        homePage.clickDetailsUser();
        DetailsUserPage detailsUserPage = new DetailsUserPage(driver);
        //проверяем, добавлен ли пользователь в группу
        detailsUserPage.checkDetails();
    }

    @Test(priority = 4, description = "Fifth test")
    public void deleteGroups(){
        driver.get("http://localhost/addressbook/group.php");
        GroupsPage groupsPage = new GroupsPage(driver);
        //удаляем группу
        groupsPage.deleteGroup();
        driver.get("http://localhost/addressbook/group.php");
        //проверяем отсутствие удаленной группы
        groupsPage.checkDeleteGroup("Select (test)");
    }

    @Test(priority = 5, description = "Sixth test")
    public void deleteUser(){
        driver.get("http://localhost/addressbook/");
        HomePage homePage = new HomePage(driver);
        //удаляем пользователя
        homePage.clickEditUser();
        AddEditNewUserForm addEditNewUserForm = new AddEditNewUserForm(driver);
        addEditNewUserForm.clickButtonDelete();
        driver.get("http://localhost/addressbook/");
        //проверяем отсутствие удаленного пользователя
        addEditNewUserForm.checkDeleteUser("Select (Boris Ivanov)");

    }

    //закрытие драйвера
    @AfterClass
    public void closeUp() {
        driver.close();
        System.out.println("The close_up process is completed");
    }

}
